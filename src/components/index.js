import React, { Component } from 'react'
import axios from 'axios'

import TypeBox from 'components/TypeBox'
import TextArea from 'components/TextArea'

const ENDPOINT = 'http://www.randomtext.me/api/'

class Index extends Component {
  constructor() {
    super()

    this.state = {
      words: [],
      startTime: '',
      endTime: '',
      currentWord: '',
      currentWordIndex: 0,
      typedWord: '',
      showResult: false,
      disabled: false,
      loading: true
    }
  }

  componentDidMount() {
    this.fetchData()
  }

  componentDidUpdate(prevState, prevProps) {
    const { words, currentWordIndex, disabled } = this.state

    if ((words.length === currentWordIndex) && !disabled) {
      this.setState({
        disabled: true,
        showResult: true,
        endTime: new Date()
      })
    }
  }

  fetchData = () => {
    axios.get(ENDPOINT)
      .then(response => {
        const text = response.data.text_out.replace(/(<([^>]+)>)/ig,""),
              words = text.split(' ')

        this.setState({
          startTime: new Date(),
          currentWord: words[0],
          words: words.slice(0, 2),
          loading: false
        })
      })
  }

  onNewWord = (word) => {
    this.setState((prevState) => {
      const { currentWord, currentWordIndex, words} = prevState

      return {
        currentWord: words[currentWordIndex + 1],
        currentWordIndex: currentWordIndex + 1,
        typedWord: ''
      }
    })
  }

  onTextChange = (value) => this.setState({ typedWord: value })

  showResult = () => {
    if (!this.state.showResult)
      return

    const { startTime, endTime, words } = this.state,
          minutes = (endTime - startTime) / 60000,
          wpm = parseInt(words.length / minutes, 10)

    return (
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <h3 className="text-center">Result</h3>
          <table>
            <tbody>
              <tr>
                <td>Your speed</td>
                <td>{wpm} WPM</td>
              </tr>
              <tr>
                <td>Time</td>
                <td>{minutes.toFixed(2)} mins</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  render() {
    const {
      words,
      currentWord,
      typedWord,
      currentWordIndex,
      disabled,
      loading
    } = this.state

    if (loading)
      return (<h3 className="text-center">Loading...</h3>)

    return (
      <div className="container">
        <TextArea
          words={this.state.words}
          currentWord={currentWord}
          typedWord={typedWord}
          currentWordIndex={currentWordIndex}
        />
        <TypeBox
          submitWord={this.onNewWord}
          onTextChange={this.onTextChange}
          currentWord={currentWord}
          typedWord={typedWord}
          disabled={disabled}
        />
        {this.showResult()}
      </div>
    )
  }
}

export default Index;
