import React from 'react'
import PropTypes from 'prop-types'

const renderWord = ({ word, typedWord, currentWord, isCurrent }) => {

  return word.split('').map((letter, key) => {
    let classList = ''

    if (isCurrent && typedWord.length === key)
      classList = 'underlined'

    if (isCurrent && typedWord[key] == word[key])
      classList = 'done'

    if (isCurrent && typedWord[key] !== word[key] && typedWord.length > key)
      classList = 'error'

    return (
      <span key={key} className={`${classList} letter`}>{letter}</span>
    )
  })
}

const renderParagraph = ({ words, currentWord, typedWord, currentWordIndex }) => {
  let flag = false

  return words.map((word, key) => {
    let classList = '',
        isCurrent = false

    if (word === currentWord && !flag) {
      isCurrent = true
      flag = !flag
    }

    if (currentWordIndex != 0) {
      classList += 'done'
      currentWordIndex--
    }

    return (
      <span key={key} className={`${classList} word`}>
        {renderWord({word, typedWord, currentWord, isCurrent})}
      </span>
    )
  })
}

const TextArea = (props) => {
  return (
    <div className="text-area-wrapper">
      <div className="text-area">
        {renderParagraph(props)}
      </div>
    </div>
  )
}

export default TextArea
