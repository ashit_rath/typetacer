import React, { Component } from 'react'
import PropTypes from 'prop-types'

class TypeBox extends Component {
  onTextChange = (event) => {
    const { value } = event.currentTarget,
          { currentWord, submitWord, onTextChange } = this.props

    if ((currentWord === value.trim()) && value[value.length - 1] === ' ')
      submitWord(value)
    else if (value.length <= currentWord.length )
      onTextChange(value)
  }

  render() {
    return (
      <input
        type="text"
        onChange={this.onTextChange}
        value={this.props.typedWord}
        className="type-box"
        disabled={this.props.disabled ? 'disabled' : ''}
      />
    )
  }
}

export default TypeBox
